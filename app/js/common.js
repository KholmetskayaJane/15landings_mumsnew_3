//burger-menu

document.addEventListener("DOMContentLoaded", function(){
	var button = document.getElementById("burger");
	var navList = document.querySelector(".header-nav-list");
	button.addEventListener('click', function(){
		navList.classList.toggle("active_burger");
	});
});

// ajax


$(document).ready(function(){
	$("#form").submit(function(e) { 
			var form = $(this);
			var data = form.serialize();			
            $.ajax({
				type: "POST", 
				url: "send.php", 
				data: data,
				success: function() {					
					$(".message").css("display", "flex");
				},
				error: function(){
					alert("Сообщение не отправлено!");
				}
			});	
			e.preventDefault();
	});	
}); 

$(document).ready(function(){
	$(".message-close").click(function() {
		$(".message").css("display", "none");
	});
}); 